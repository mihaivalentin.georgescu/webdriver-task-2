describe("Test suite for https://pastebin.com/ website", () => {
    it("First test", async () => {
        await browser.navigateTo("https://pastebin.com/");
        await browser.pause(1000);

        const button_agree = await $("//button[@mode='primary']");
        const textarea = await $("textarea#postform-text");
        const page_expiration_never = await $("//span[text()='Never']");
        const page_expiration_10_minutes = await $("//li[text()='10 Minutes']");
        const post_form = await $("//input[@id='postform-name']");
        const create_paste_button = await $("//button[text()='Create New Paste']");
        const syntax_highlight = await $("//div[@class='form-group field-postform-format']//b[@role='presentation']");
        const bash_highlight = await $("//li[text()='Bash']");

        button_agree.click();
        await browser.pause(1000);

        await textarea.setValue("git config --global user.name  \"New Sheriff in Town\"");

        // Input the second command
        await textarea.addValue("\n"); // Add a newline character
        await textarea.addValue("git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")");

        // Input the third command
        await textarea.addValue("\n"); // Add another newline character
        await textarea.addValue("git push origin master --force");

        await browser.execute(() => {
            window.scrollTo(0, 500);
        });

        syntax_highlight.click();
        await browser.pause(1000);
        bash_highlight.click();
        await browser.pause(1000);
        page_expiration_never.click();
        await browser.pause(1000);
        page_expiration_10_minutes.click();
        await browser.pause(1000);
        post_form.addValue("how to gain dominance among developers");
        await browser.pause(9000);
        // Input the first command
       

        await browser.execute(() => {
            window.scrollTo(0, 1000);
        });
        
        create_paste_button.click();
        await browser.pause(5000);

        const page_title = await $("//h1[text()='how to gain dominance among developers']");
        const page_title_text = await page_title.getText(); 

        expect(page_title_text).toEqual("how to gain dominance among developers"); 

        const bash_highlight_text = await $("//a[text()='Bash']")
        const bash_highlight_text_verify = await bash_highlight_text.getText(); 

        expect(bash_highlight_text_verify).toEqual("Bash"); 

        const codeElements = await $$(".li1");

        let textFound = false;

        for (const codeElement of codeElements) {

            const codeText = await codeElement.getText();

            if (codeText.includes("git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")")) {
                textFound = true;
                break;
            }
        }

        expect(textFound).toBe(true);

    });

});